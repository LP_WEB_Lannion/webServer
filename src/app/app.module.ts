import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { JqueryMainComponent } from './components/jquery-main/jquery-main.component';
import { AngularMainComponent } from './components/angular-main/angular-main.component';
import { HomeComponent } from './components/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { DownloadComponent } from './components/common/download/download.component';
import { MatIconModule } from '@angular/material/icon';
import { SwaggerComponent } from './components/swagger/swagger.component';
import { SafePipe } from './pipes/safe.pipe';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { FrameworksComponent } from './components/frameworks/frameworks.component';

const appRoutes: Routes = [
  { path: 'webApp/jquery', component: JqueryMainComponent },
  { path: 'webApp/angular', component: AngularMainComponent },
  { path: 'webApp/frameworks', component: FrameworksComponent },
  { path: 'webApp/home', component: HomeComponent },
  { path: 'swagger', component: SwaggerComponent },
  {
    path: '',
    redirectTo: '/webApp/home',
    pathMatch: 'full'
  },
  {
    path: 'webApp',
    redirectTo: '/webApp/home',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    JqueryMainComponent,
    AngularMainComponent,
    HomeComponent,
    DownloadComponent,
    SwaggerComponent,
    SafePipe,
    FrameworksComponent
  ],
  imports: [
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule,
    MatListModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
