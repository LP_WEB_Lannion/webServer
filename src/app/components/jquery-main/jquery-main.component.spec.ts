import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JqueryMainComponent } from './jquery-main.component';

describe('JqueryMainComponent', () => {
  let component: JqueryMainComponent;
  let fixture: ComponentFixture<JqueryMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JqueryMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JqueryMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
