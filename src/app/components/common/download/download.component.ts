import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css']
})
export class DownloadComponent implements OnInit {
  @Input()
  iframe: boolean;
  @Input()
  title: string;
  @Input()
  link: string;
  @Input()
  directDownload: boolean;
  safelink:SafeResourceUrl;
  displayIframe=false;
  constructor(private domsan:DomSanitizer) {}

  ngOnInit() {
    this.safelink=this.domsan.bypassSecurityTrustResourceUrl(this.link)
    if (this.directDownload == null) {
      this.directDownload = true;
    }
  }
}
