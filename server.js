var express = require("express");
var bodyParser = require("body-parser");
var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;
const mongoose = require("mongoose");
var swaggerJSDoc = require("swagger-jsdoc");
var path = require("path");
var app = express();
app.use(bodyParser.json());

// Create link to Angular build directory
var distDir = __dirname + "/dist/";
var staticDir = __dirname + "/static/";

app.use(express.static(staticDir));
app.use(express.static(distDir));

// Add headers
app.use(function(req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});

require("./src_server/api")(app);
require("./src_server/routes")(app);
require("./src_server/pokemonApi")(app);
app.get("/swagger.json", function(req, res) {
  res.setHeader("Content-Type", "application/json");
  res.send(swaggerSpec);
});
app.get("/swagger/", function(req, res) {
  res.sendFile(path.join(__dirname, "/static/swagger/", "index.html"));
});

var swaggerSpec = swaggerJSDoc({
  swaggerDefinition: {
    info: {
      title: "Swagger",
      version: "1.0.0"
      // description: 'Dobby Family ',
    },
    //host: Config.server.host+":"+Config.server.port,
    basePath: "/"
    //schemes : [ "http" ],
  },
  // path to the API docs
  apis: [__dirname + "/src_server/api.js",__dirname + "/src_server/pokemonApi.js"]
});

console.log([__dirname + "/src_server/api.js"]);
// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
var db;
mongoose.Promise = Promise;

try {
  // Connect to the database before starting the application server.
  mongoose.connect(
    process.env.MONGODB_URI || "mongodb://localhost:27017/lpweb",
    function(err, client) {
      if (err) {
        console.log(err);
        process.exit(1);
      }

      // Save database object from the callback for reuse.
      db = mongoose.connection;
      console.log("Database connection ready");
      db.on("error", console.error.bind(console, "connection error:"));
      // Initialize the app.
      var server = app.listen(process.env.PORT || 8080, function() {
        var port = server.address().port;
        console.log("App now running on port", port);
      });
    }
  );
} catch (error) {}
