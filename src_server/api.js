const https = require('https');
var mongoose = require('mongoose');
const bcrypt = require('bcrypt');
mongoose.set('debug', false);
var pokemonSchema = mongoose.Schema(
  {name: {type: String, unique: true}},
  {collection: 'pokemon', strict: false},
);
var tubeSchema = mongoose.Schema(
  {title: {type: String, unique: true}},
  {collection: 'tube', strict: false},
);

var pokemon = mongoose.model('pokemon', pokemonSchema);
var tube = mongoose.model('tube', tubeSchema);

function handleError(res, reason, message, code) {
  console.log('ERROR: ' + reason);
  res.status(code || 500).json({error: message});
}
//
module.exports = function(app) {
  /**
   * @swagger
   * /api/deezer/populate/{key}:
   *   post:
   *     summary: populate database
   *     tags:
   *        - deezer
   *     parameters:
   *        - in: path
   *          name: key
   *          schema:
   *             type: string
   *          required: true
   *          description: the admin key
   *     description: Returns an array track.
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: populate in progress
   *
   *       403:
   *         description: unauthorized
   *       500:
   *         description: server error
   *
   */
  app.post('/api/deezer/populate/:key', function(req, res) {
    var key = req.params.key;
    var hash = '$2b$10$z3tWjZ8pbxaipdcmWQAGeejTtJjG1qHDtaW61skEVrcZdOMffY2Ca';

    if (bcrypt.compareSync(key, hash) == false) {
      res.status(403);
      res.end();
      return;
    } else {
      var alphabet1 = 'abcdefghijklmnopqrstuvwxyz';
      var alphabet = [];
      for (var n = 0; n < alphabet1.length; n++) {
        var newindex = Math.floor(Math.random() * alphabet1.length);
        console.log(newindex);
        alphabet.push(alphabet1[newindex]);
      }

      for (var i = 0; i < 10; i++) {
        for (var j = 0; j < 10; j++) {
          console.log('deezer request for ', alphabet[i] + alphabet[j]);
          https
            .get(
              'https://api.deezer.com/search?q=track:"' +
                alphabet[i] +
                alphabet[j] +
                '"&order=RATING_DESC',
              resp => {
                var data = '';
                // A chunk of data has been recieved.
                resp.on('data', chunk => {
                  data += chunk;
                });
                // The whole response has been received. Print out the result.
                resp.on('end', () => {
                  var mytube = JSON.parse(data);
                  if (mytube.data && mytube.data.length > 0) {
                    mytube.data.forEach(element => {
                      var newtube = new tube(element);
                      newtube.save(function(err, results) {
                        if (!err)
                          console.log('Title ' + results.title + ' in base');
                        else console.log(err.message);
                      });
                    });
                  }
                });
              },
            )
            .on('error', err => {
              console.log('Error: ' + err.message);
            });
        }
      }
      res.send('populate in progress');
      res.status(200);
      res.end();
    }
  });
  /**
   * @swagger
   * /api/pokemon/populate/{key}/{startIndex}/{maxNum}:
   *   post:
   *     summary: populate database
   *     tags:
   *        - pokemon
   *     parameters:
   *        - in: path
   *          name: key
   *          schema:
   *             type: string
   *          required: true
   *          description: the admin key
   *        - in: path
   *          name: startIndex
   *          schema:
   *             type: integer
   *          required: true
   *          description: the starting index of pokemon
   *        - in: path
   *          name: maxNum
   *          schema:
   *             type: integer
   *          required: true
   *          description: the number of pokemons
   *     description: Returns 200 ok if ok
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: populate in progress
   *       403:
   *         description: unauthorized
   *
   */
  app.post('/api/pokemon/populate/:key/:startIndex/:maxNum/', function(
    req,
    res,
  ) {
    // try to initialize the db on every request if it's not already
    // initialized.

    var max = parseInt(req.params.maxNum);
    var index = parseInt(req.params.startIndex);
    var key = req.params.key;

    if (key != process.env.POPULATE_KEY) {
      res.status(403);
      res.end();
      return;
    }

    for (var i = index; i < index + max; i++) {
      https
        .get('https://pokeapi.co/api/v2/pokemon/' + i + '/', resp => {
          var data = '';
          // A chunk of data has been recieved.
          resp.on('data', chunk => {
            data += chunk;
          });
          // The whole response has been received. Print out the result.
          resp.on('end', () => {
            var mypoke = JSON.parse(data);
            console.log(mypoke.location_area_encounters.split('/'));

            var newPokemon = {
              poke_id: parseInt(mypoke.location_area_encounters.split('/')[6]),
              name: mypoke.name,
              image: mypoke.sprites.front_default,
              type: mypoke.types[0].type.name,
              stats: {
                attack: mypoke.stats[4].base_stat,
                hp: mypoke.stats[5].base_stat,
                speed: mypoke.stats[0].base_stat,
                defense: mypoke.stats[3].base_stat,
              },
            };
            //  console.log(newPokemon);
            // var newpoke = new pokemon(newPokemon);

            pokemon.deleteOne({name: newPokemon.name}, function(err, results) {
              var newpoke = new pokemon(newPokemon);
              newpoke.save(function(err, results) {
                if (!err)
                  console.log(
                    'Pokemon ' + JSON.stringify(results) + ' in base',
                  );
                else console.log(err.message);
              });
            });
          });
        })
        .on('error', err => {
          console.log('Error: ' + err.message);
        });
    }
    res.status(200);
    res.end();
  });

  /**
   * @swagger
   * /api/pokemon/{id}:
   *   get:
   *     summary: get a pokemon
   *     tags:
   *        - pokemon
   *     parameters:
   *        - in: path
   *          name: id
   *          schema:
   *             type: integer
   *          required: true
   *          description: Numeric ID of the user to get
   *     description: Returns a pokemon.
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: a pokemon
   *
   *       403:
   *         description: unauthorized
   *       500:
   *         description: server error
   *
   */
  app.get('/api/pokemon/:id/', function(req, res) {
    var id = parseInt(req.params.id);
    pokemon.findOne({poke_id: id}, function(err, pokemon) {
      if (err) handleError(res, err.reason, err.message, err.code);
      else res.send(pokemon);
    });
  });

  /**
   * @swagger
   * /api/deezer/search:
   *   get:
   *     summary: get an array of track
   *     tags:
   *        - deezer
   *     parameters:
   *        - in: query
   *          name: q
   *          schema:
   *             type: string
   *          required: true
   *          description: the beginning chars of the track
   *     description: Returns an array track.
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: an array of track
   *
   *       403:
   *         description: unauthorized
   *       500:
   *         description: server error
   *
   */
  app.get('/api/deezer/search/', function(req, res) {
    var target = req.query.q.toLowerCase();
    var regxep = new RegExp('^' + target, 'i');
    tube
      .find({title: regxep})
      .limit(10)
      .sort({rank: -1})
      .exec(function(err, titles) {
        if (err) handleError(res, err.reason, err.message, err.code);
        else
          res.send(
            titles.map(title => {
              var filterd = {
                id: title._id,
                title: title.title,
              };
              return filterd;
            }),
          );
      });
  });

  /**
   * @swagger
   * /api/deezer/track/{id}:
   *   get:
   *     summary: get an array of track
   *     tags:
   *        - deezer
   *     parameters:
   *        - in: path
   *          name: id
   *          schema:
   *             type: string
   *          required: true
   *          description: the beginning chars of the track
   *     description: Returns an array track.
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: a track
   *
   *       403:
   *         description: unauthorized
   *       500:
   *         description: server error
   *
   */
  app.get('/api/deezer/track/:id/', function(req, res) {
    var target = req.params.id;

    tube.findById(target).exec(function(err, titles) {
      if (err) handleError(res, err.reason, err.message, err.code);
      else res.send(titles);
    });
  });
};
