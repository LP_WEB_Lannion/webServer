const https = require('https');
var mongoose = require('mongoose');
const bcrypt = require('bcrypt');
mongoose.set('debug', false);
var jwt = require('jsonwebtoken');

var privateKey = 'pokemonAPIkey';

var userSchema = mongoose.Schema(
  {name: {type: String, unique: true}, coins: Number, deck: [Number]},
  {collection: 'user', strict: false},
);
var user = mongoose.model('user', userSchema);

function handleError(res, reason, message, code) {
  console.log('ERROR: ' + reason);
  res.status(code || 500).json({error: message});
}

module.exports = function(app) {
  /**
   * @swagger
   * /api/user/login:
   *   post:
   *     summary: login to pokemen and get a token
   *     tags:
   *        - pokemon
   *
   *     parameters:
   *        - in: body
   *          name: user
   *          schema:
   *             type: object
   *             properties:
   *                name:
   *                   type: string
   *          required: true
   *          description: user
   *     description: Returns a user token.
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *                  schema:
   *                     type: object
   *                     properties:
   *                        token:
   *                            type: string
   *                        description: the user's token
   *
   *       403:
   *         description: unauthorized
   *       500:
   *         description: server error
   *
   */
  app.post('/api/user/login', async (req, res) => {
    var token = jwt.sign(req.body, privateKey);
    // create it if not exists
    let existinguser = await user
      .findOne({name: req.body.name})
      .catch(err => handleError(res, err.reason, err.message, err.code));
    if (!existinguser) {
      await user
        .create({
          name: req.body.name,
          coins: 100,
          deck: [],
        })
        .catch(err => handleError(res, err.reason, err.message, err.code));
    }
    res.send({token: token});
  });

  /**
   * @swagger
   * /api/user:
   *   get:
   *     summary: get user with a token
   *     tags:
   *        - pokemon
   *     parameters:
   *        - in: header
   *          name: authorization
   *          type: string
   *          required: true
   *          description: token
   *     description: Returns an user .
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         schema:
   *                     type: object
   *                     properties:
   *                        name:
   *                            type: string
   *                        description: the user
   *                        coins:
   *                            type: number
   *                        deck:
   *                              type: array
   *                              items:
   *                                  type: number
   *       403:
   *         description: unauthorized
   *       500:
   *         description: server error
   *
   */
  app.get('/api/user/', async (req, res) => {
    if (!req.headers.authorization) {
      res.status(403).send('forbidden');
      return;
    }
    var myuser = jwt.decode(req.headers.authorization);
    if (!myuser) {
      res.status(403).send('forbidden');
      return;
    }
    let existinguser = await user.findOne({
      name: myuser.name,
    });
    if (!existinguser) {
      res.status(403).send('forbidden');
      return;
    }
    res.send(existinguser);
  });

  /**
   * @swagger
   * /api/user:
   *   put:
   *     summary: update user with a token
   *     tags:
   *        - pokemon
   *     parameters:
   *        - in: header
   *          name: authorization
   *          type: string
   *          required: true
   *          description: token
   *        - in: body
   *          name: user
   *          schema:
   *             type: object
   *             properties:
   *                coins:
   *                   type: number
   *                deck:
   *                   type: array
   *                   items:
   *                      type: number
   *          required: true
   *          description: user
   *     description: Returns an user .
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: success
   *       403:
   *         description: unauthorized
   *       500:
   *         description: server error
   *
   */
  app.put('/api/user/', async (req, res) => {
    if (!req.headers.authorization) {
      res.status(403).send('forbidden');
      return;
    }
    var myuser = jwt.decode(req.headers.authorization);
    if (!myuser) {
      res.status(403).send('forbidden');
      return;
    }
    let existinguser = await user.findOne({
      name: myuser.name,
    });
    if (!existinguser) {
      res.status(403).send('forbidden');
      return;
    }
    let updateduser = await user
      .update(
        {name: myuser.name},
        {
          name: myuser.name,
          coins: req.body.coins,
          deck: req.body.deck,
        },
      )
      .catch(err => handleError(res, err.reason, err.message, err.code));
    res.send(updateduser);
  });

  /**
   * @swagger
   * /api/user:
   *   delete:
   *     summary: delete user with a token
   *     tags:
   *        - pokemon
   *     parameters:
   *        - in: header
   *          name: authorization
   *          type: string
   *          required: true
   *          description: token
   *     description: delete an user .
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: success
   *
   *       403:
   *         description: unauthorized
   *       500:
   *         description: server error
   *
   */
  app.delete('/api/user/', async (req, res) => {
    if (!req.headers.authorization) {
      res.status(403).send('forbidden');
      return;
    }

    var myuser = jwt.decode(req.headers.authorization);
    if (!myuser) {
      res.status(403).send('forbidden');
      return;
    }
    let existinguser = await user.findOne({
      name: myuser.name,
    });
    if (!existinguser) {
      res.status(403).send('forbidden');
      return;
    }
    let updateduser = await user
      .deleteOne({name: myuser.name})
      .catch(err => handleError(res, err.reason, err.message, err.code));
    res.send(updateduser);
  });
};
